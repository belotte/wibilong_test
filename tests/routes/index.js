describe('/api/v1/shibes', async () => {
	it(`/protocol=http&url=true&count=1`, async () => {
		const { body, status_code } = await requests.get({
			url: `${hosts.local}/api/v1/shibes?protocol=http&url=true&count=1`
		});

		expect(body.errors).to.equal(undefined);
		expect(status_code).to.equal(200);
		expect(body.total).to.equal(1);
		expect(body.data.shibes.length).to.equal(1);
		expect(body.data.shibes[0].url).to.match(/^http:/);
	});
	it(`/protocol=http&url=true&count=10`, async () => {
		const { body, status_code } = await requests.get({
			url: `${hosts.local}/api/v1/shibes?protocol=http&url=true&count=10`
		});

		expect(body.errors).to.equal(undefined);
		expect(status_code).to.equal(200);
		expect(body.total).to.equal(10);
		expect(body.data.shibes.length).to.equal(10);
		expect(body.data.shibes[0].url).to.match(/^http:/);
	});
	it(`/protocol=https&url=true&count=1`, async () => {
		const { body, status_code } = await requests.get({
			url: `${hosts.local}/api/v1/shibes?protocol=https&url=true&count=1`
		});

		expect(body.errors).to.equal(undefined);
		expect(status_code).to.equal(200);
		expect(body.total).to.equal(1);
		expect(body.data.shibes.length).to.equal(1);
		expect(body.data.shibes[0].url).to.match(/^https:/);
	});
	it(`/protocol=http&url=false&count=1`, async () => {
		const { body, status_code } = await requests.get({
			url: `${hosts.local}/api/v1/shibes?protocol=http&url=false&count=1`
		});

		expect(body.errors).to.equal(undefined);
		expect(status_code).to.equal(200);
		expect(body.total).to.equal(1);
		expect(body.data.shibes.length).to.equal(1);
		expect(body.data.shibes[0].id).to.match(/^[a-z0-9]*$/);
	});
});
describe('/api/v1/animals', async () => {
	it(`/protocol=http&url=false&count=1`, async () => {
		const { body, status_code } = await requests.get({
			url: `${hosts.local}/api/v1/animals?protocol=http&url=false&count=1`
		});

		expect(body.errors).to.equal(undefined);
		expect(status_code).to.equal(200);
		expect(body.total).to.equal(1);
		expect(body.data.animals.length).to.equal(1);
		expect(body.data.animals[0].id).to.match(/^[a-z0-9]*$/);
		expect(['shibes', 'birds', 'cats']).to.include(body.data.animals[0].type);
	});
});

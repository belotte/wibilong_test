require(`${process.env.PWD}/index`)
global.chai = require('chai')
global.should = chai.should()
global.assert = chai.assert
global.expect = chai.expect
let server_ready = false

process.on('server_started', () => { server_ready = true })
describe('Tests', function () {
	before(function (done) {
		setTimeout(() => {
			done()
		}, !server_ready * 15 * 1000)
		process.on('server_started', () => {
			done()
		})
	})

	this.timeout(60 * 1000)

	let files_to_test = {
		/*	[name, path, enabled] */
		routes: [
			['index',	'routes/index',		true]
		]
	}

	require('colors').setTheme({
		bold_blue: ['blue', 'bold'],
		dim_gray: ['dim', 'gray'],
	})

	for (let key of Object.keys(files_to_test)) {
		console.info(key)
		describe(key, () => {
			for (let file of files_to_test[key]) {
				console.info(`  ${file[0]} ${file[2] && 'enabled'.green || 'disabled'.gray}`)
				if (!file[2]) { continue }
				describe(file[0], () => {
					require(`./${file[1]}`)
				})
			}
		})
	}

	after(() => {
		setTimeout(() => {
			process.exit()
		}, 500)
	})
})

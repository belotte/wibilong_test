require('colors');

global.moment = require('moment');
global.requests = require(`${process.env.PWD}/api/services/async_request`);
global.hosts = require(`${process.env.PWD}/config/hosts`);

global.now = date => moment(date).format('YYYY-MM-DD HH:mm:ss');
global.now_minimal = date => moment(date).format('YYMMDD_HHmmss_SSS');

global.require_and_reload = path => {
	try {
		delete require.cache[require.resolve(path)];
	} catch (error) { }
	return require(path);
};

global.wait = async (delay) => new Promise (resolve => { setTimeout(() => resolve(), delay * 1000) });

Object.defineProperty(Object.prototype, 'stringified_query_parameters', {
	get: function () {
		let result = '';
		for (let key in this) {
			result = `${result}${key}=${this[key]}&`;
		}
		return result.replace(/&$/, '');
	}
});

const loggers = {
	info: console.info,
	error: console.error,
	function_called: console.info
};

for (let type in loggers) {
	console[type] = function () {
		args = Object.keys(arguments).map(id => arguments[id]).map(arg => {
			if (typeof arg === 'object') {
				return JSON.parse(`${JSON.stringify(arg, null, 2)}`);
			}
			return (arg || '').toString().replace(/^"|"$/g, '');
		})
		let stack_component = new Error().stack.split('at ')[2].replace(/\n/g, '');
		let caller_components = {
			filename: stack_component.match(/\/([0-9a-zA-Z_]*\.js)/)[1].replace(process.env.PWD, ''),
			function: stack_component.split(' ')[0].includes(process.env.PWD) && '-' || stack_component.split(' ')[0],
			line: stack_component.match(/:([0-9]*:[0-9]*)/)[1]
		};

		loggers[type].apply(console, [`[${now_minimal()}]${['error', 'function_called'].includes(type) && ` [${caller_components.filename}:${caller_components.line.split(':')[0]}][${caller_components.function.replace(/^Object\./, '')}]` || ''}`[{
			info: 'gray',
			error: 'red',
			function_called: 'blue'
		}[type]], ...args]);

		if (['error'].includes(type) && arguments[0] && typeof arguments[0] === 'object') {
			arguments[0].logged = true;
		}

		return Object.keys(arguments).map(id => arguments[id])[0];
	};
}

if (process.env.DO_NOT_MODIFY_LOGGERS) {
	console.info = loggers.info;
	console.error = loggers.error;
	if (!process.env.LOG_FUNCTIONS) {
		console.function_called = () => { };
	}
}

const query_parameters_checker = (req, res, next) => {
	const errors = []
	if (!req.query.protocol || !['http', 'https'].includes(req.query.protocol)) {
		errors.push('protocol');
	}
	if (req.query.count && (req.query.count / 1 < 1 && req.query.count / 1 > 100)) {
		errors.push('count');
	}
	if (!req.query.url || !['true', 'false'].includes(req.query.url)) {
		errors.push('url');
	}

	if (errors.length) {
		throw [{
			param: 'protocol',
			message: 'Must be one of `http` or `https`.'
		}, {
			param: 'count',
			message: 'If specified, must  be number between `1` and `100`.'
		}, {
			param: 'url',
			message: 'Must be one of `true` or `false`.'
		}].filter(({ param: error }) => errors.includes(error));
	}

	return next();
}

module.exports = {
	query_parameters_checker
};

const router = require('express').Router();
const mdw = require(`${process.env.PWD}/api/services/middlewares`);
const enabled_animals = ['shibes', 'cats', 'birds'];

router.route('/animals')
.get(mdw.query_parameters_checker, async(req, res) => {
	let elements_to_pick = enabled_animals.reduce((result, animal) => {
		result[animal] = 0;
		return result;
	}, {});
	let pictures = [];

	for (let i = 0; i < (req.query.count / 1 || 1); i++) {
		elements_to_pick[enabled_animals[Math.random() * (enabled_animals.length) | 0]]++;
	}

	await Promise.all(enabled_animals.map(category => new Promise(async(resolve, reject) => {
		if (!elements_to_pick[category]) { return resolve(); }
		const { body } = await requests.get({
			url: `${hosts.local}/api/v1/${category}?protocol=${req.query.protocol}&count=${elements_to_pick[category]}&url=${req.query.url}`
		});

		if (body.errors) {
			throw body.errors;
		}

		pictures = [...pictures, ...body.data[category].map(element => ({
			type: category,
			...element
		}))];

		return resolve();
	})));

	return res.json({
		type: 'success',
		code: 200,
		total: pictures.length,
		data: {
			animals: pictures
		}
	});
})

router.route(`/:animal_category(${enabled_animals.join('|')})`)
.get(mdw.query_parameters_checker, async(req, res) => {
	const errors = [];

	req.query.count = req.query.count / 1 || 1;
	req.query.url = req.query.url !== 'false';

	const { body } = await requests.get({
		url: `http://shibe.online/api/${req.params.animal_category}?count=${req.query.count}&urls=${req.query.url}&httpsUrls=${req.query.protocol === 'https'}`
	});

	return res.json({
		type: 'success',
		code: 200,
		total: body.length,
		data: {
			[req.params.animal_category]: body.map(element => ({ [req.query.url && 'url' || 'id']: element }))
		}
	});
})

module.exports = router;

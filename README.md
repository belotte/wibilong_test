# Shibes Fetcher
This API is available here: http://wibilong.frank42.fr
Exemple: http://wibilong.frank42.fr/api/v1/animals?protocol=http&url=true&count=1
## Installation
```bash
git clone https://gitlab.com/belotte/wibilong_test.git
cd wibilong_test
npm Install
npm start
```
## API
```
GET /api/v1/shibes  # returns shibes pictures
GET /api/v1/cats    # returns cats pictures
GET /api/v1/birds   # returns birds pictures
GET /api/v1/animals # return random shibes, cats and birds pictures
```
#### Query parameters
**protocol** *required* (`http`|`https`): HTTP protocol of fetched ressources.
**count** (`1`-`100`; default `1`): quantity of images to fetch.
**url** (`true`|`false`; default `true`): if `true`, returns URLs. Else, returns IDs.
### Possible JSON outputs:
#### Success
```javascript
{
	type: 'success',
	code: 200,
	total: 1,
	data: {
		// GET /api/v1/shibes
		shibes: [{
			// if url = false
			id: '404ae6ed024aa5f1b9001fd755abdebe87886a0e',
			// if url = true
			url: 'http://cdn.shibe.online/shibes/404ae6ed024aa5f1b9001fd755abdebe87886a0e.jpg'
		}],
		// GET /api/v1/cats
		cats: [{
			// if url = false
			id: '404ae6ed024aa5f1b9001fd755abdebe87886a0e',
			// if url = true
			url: 'http://cdn.shibe.online/shibes/404ae6ed024aa5f1b9001fd755abdebe87886a0e.jpg'
		}],
		// GET /api/v1/birds
		birds: [{
			// if url = false
			id: '404ae6ed024aa5f1b9001fd755abdebe87886a0e',
			// if url = true
			url: 'http://cdn.shibe.online/shibes/404ae6ed024aa5f1b9001fd755abdebe87886a0e.jpg'
		}],
		// GET /api/v1/animals
		animals: [{
			type: 'shibe',
			// if url = false
			id: '404ae6ed024aa5f1b9001fd755abdebe87886a0e',
			// if url = true
			url: 'http://cdn.shibe.online/shibes/404ae6ed024aa5f1b9001fd755abdebe87886a0e.jpg'
		}]
	}
}
```
#### Bad request
```javascript
{
	type: 'badRequest',
	code: 400,
	errors: [{
		param: 'protocol',
		message: ERROR_MESSAGE
	}, {
		param: 'count',
		message: ERROR_MESSAGE
	}, {
		param: 'url',
		message: ERROR_MESSAGE
	}]
}
```

Object.keys(require('./.env')).map(key => process.env[key.toUpperCase()] = require('./.env')[key]);

[{
	condition: !process.env.NODE_ENV,
	error: 'Environment variable NODE_ENV must be define.',
	code: 101
}, {
	condition: !['production', 'development', 'test'].includes(process.env.NODE_ENV),
	error: 'Environment variable NODE_ENV must be one of production, development or test.',
	code: 102
}, {
	condition: !process.env.PORT,
	error: 'Environment variable PORT must be define.',
	code: 103
}, {
	condition: !parseInt(process.env.PORT),
	error: 'Environment variable PORT must be a number.',
	code: 104
}].forEach(({ condition, error, code }) => {
	if (condition) {
		console.error(error);
		process.exit(code);
	}
});

require(`${process.env.PWD}/api/services/globals`);
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const Logger = require(`${process.env.PWD}/api/services/logger`);
const loggers = {
	requests: Logger ({
		path: 'requests.json',
		is_request: true
	})
};

app.use ('*', (req, res, next) => {
	loggers.requests.log(req);

	return next();
})

app.use('/api/v1', require(`${process.env.PWD}/routes/index`));

app.use((error, req, res, _) => {
	console.error(error);
	return res.status(400).json({
		type: 'badRequest',
		code: 400,
		errors: error
	});
});

app.use('*', (req, res) => {
	return res.status(404).end('404. Nothing found here.');
});

server.listen(process.env.PORT, async () => {
	console.info(`Server running on port ${process.env.PORT.toString().cyan.bold}.`);
	console.info(`Environment: ${process.env.NODE_ENV.toUpperCase()[{
		test: 'blue',
		development: 'green',
		production: 'red'
	}[process.env.NODE_ENV] || 'blue'].bold}.`);

	await wait(2);

	process.emit('server_started');
});
